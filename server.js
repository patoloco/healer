const express = require('express');

module.exports = {
  start: (port) => {
    const app = express();

    app.get('/', (req, res) => {
      const response = `Worker ${process.pid} processed a request at ${new Date().getTime()}`;
      process.send(response);
      res.send(response);
    });

    app.get('/kill', (req, res) => {
      const response = `Killing worker ${process.pid} at ${new Date().getTime()}`;
      process.send(response);
      res.send(response);
      process.exit(0);
    });

    app.get('/killall', (req, res) => {
      res.send();
      process.send('KILLALL');
    });

    app.get('/badpromise', (req, res) => {
      Promise.reject('bad, bad promise');
    });

    app.listen(port);
    process.send(`Worker ${process.pid} started, listening on port ${port}`);
  },
};
