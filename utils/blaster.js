const http = require('http');

const errorRate = 2;
const totalMs = 20 * 1000;
const intervalMs = 15;

const results = {
  success: 0,
  fail: 0,
  attempts: 0,
};

const reqInterval = setInterval(() => {
  results.attempts++;

  if (Math.floor(Math.random() * 100) < errorRate) {
    const req = http.request('http://localhost:3000/badpromise', (res) => {
      res.on('data', (data) => {
        console.log('http data in BAD req');
        results.success++;
      });
    });

    req.on('error', (e) => {
      results.fail++;
    });

    req.end();
  } else {
    const req = http.request('http://localhost:3000/', (res) => {
      res.on('data', (data) => {
        results.success++;
      });
    });

    req.on('error', (error) => {
      console.error('http error in GOOD req');
      results.fail++;
    });

    req.end();
  }
}, intervalMs);

setTimeout(() => {
  clearInterval(reqInterval);

  console.log(`\n${results.attempts} requests were made in ${totalMs}ms at a request interval of ${intervalMs}ms`);
  console.log(`${results.success} requests were successful and ${results.fail} failed`);

  const unfinishedRequests = results.attempts - (results.fail + results.success);
  if (unfinishedRequests > 0) {
    console.log(`${unfinishedRequests} request(s) did not finish`);
  }

  const actualErrorRate = 100 * (results.fail / (results.fail + results.success));
  console.log(`error rate of ${errorRate}% was specified, error rate of ${actualErrorRate.toFixed(2)}% resulted\n`);
}, totalMs);
