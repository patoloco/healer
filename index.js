const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const config = require('./config');
const server = require('./server');

if (process.argv.includes('--no-console')) {
  console.log = console.error = () => null;
}

let workerRestarts = 0;

/* MASTER setup */
if (cluster.isMaster) {
  console.log('master pid:', process.pid);

  const numWorkers = Math.min(numCPUs, config.MAX_WORKERS);
  for (let i = 0; i < numWorkers; i++) { newWorker(); }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} exited with code ${code}, signal ${signal}`);
    if (config.RESTART === true) restartWorker();
  });
}

function masterListener(msg) {
  console.log('master received msg:', msg);
  if (msg === 'KILLALL') {
    console.warn(`killing all workers, total worker restarts this run: ${workerRestarts}`);
    process.exit(0);
  }
}

/* WORKER setup */
if (cluster.isWorker) {
  process.on('message', msg => workerListener(msg));

  process.on('unhandledRejection', (e) => {
    console.error('unhandledRejection e:', e);

    // log to CloudWatch, send a notification using SNS, etc

    process.exit(1);
  });

  server.start(config.PORT);
}

function newWorker() {
  const worker = cluster.fork();
  worker.on('message', msg => masterListener(msg));
}

function restartWorker() {
  console.log('restarting worker');
  newWorker();
  workerRestarts++;

  const workerIds = Object.keys(cluster.workers);
  const currentPids = [];
  workerIds.forEach((id) => {
    const pid = cluster.workers[id].process.pid;
    currentPids.push(pid);
  });
  console.log('current pids:', currentPids);
}

function workerListener(msg) {
  console.log(`worker ${process.pid} received msg:`, msg);
}
