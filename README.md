# SUMMARY #
A basic express server that uses the NodeJS cluster module.

# RUN #
npm start - fire up the server  
npm test - blast the server with requests (run npm start first)

# CONFIG #
Set the port, maximum number of workers, and auto-restart option in config.js  
Specify the error rate, total time to blast, and interval between http requests in utils/blaster.js
